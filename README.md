# Cloud Gaming Software
This program manages users and cloud resources for any Game as a Service (GaaS) system.
For an effective use of cloud resources, each game must have a corresponding list of resources needed to play it properly.
The GaaS system then can consult all the games currently in use and allocate only the needed resources.
Each user has personal information, can have a current game selected to play and has playlists of games he wants to/can play.

## Compiling the source
The dependencies of the project are managed by Maven.
To compile the code install Maven and run it in the directory of the POM file with the following command:
<br />
`
mvn clean install
`

## Running the software
Once compiled the software can be run with Java using the following command:
<br />
`
java cloudgaming-0.0.2.jar
`
<br />
You must specify one of the following options:
<br />-f_games to list all the existing games
<br />-f_users to list all the existing users

Obviously, a program with only these 2 options is not really useful. The Cloud Gaming Software has been created to be used as library for a bigger system and all the necessary service methods can be called by the classes than need them.
Eventually a proper GUI can be developed or more line commands can be defined using Apache CLI library. 