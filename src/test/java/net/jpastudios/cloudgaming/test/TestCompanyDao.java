package net.jpastudios.cloudgaming.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.dao.CompanyDao;
import net.jpastudios.cloudgaming.model.Company;

import org.junit.Test;

public class TestCompanyDao extends TestHibernate {
	CompanyDao companyDao;

	@Override
	public void setUp() {
		super.setUp();
		companyDao = new CompanyDao();
	}
	
	/** Test for CRUD operations. */
	@Test
	public void generalTest() {
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		
		Company entity = new Company();
		entity.setName("test0");
		companyDao.save(entity);
		
		Company entity2 = new Company();
		entity2.setName("test1");
		companyDao.save(entity2);
		
		assertEquals("test0", companyDao.findByID(entity.getId_company()).getName());
		assertEquals("test1", companyDao.findByID(entity2.getId_company()).getName());
		
		entity.setName("test0Update");
		companyDao.update(entity);
		
		entity2.setName("test1Update");
		companyDao.update(entity2);
		
		assertEquals("test0Update", companyDao.findByID(entity.getId_company()).getName());
		assertEquals("test1Update", companyDao.findByID(entity2.getId_company()).getName());
		
		assertEquals(2, companyDao.findAll().size());
		
		companyDao.delete(entity);
		
		assertNull(companyDao.findByID(entity.getId_company()));
		
		companyDao.deleteAll();
		
		assertEquals(0, companyDao.findAll().size());
		
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
	}
}
