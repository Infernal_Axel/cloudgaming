package net.jpastudios.cloudgaming.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import net.jpastudios.cloudgaming.OS;
import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.dao.ServerResourcesDao;
import net.jpastudios.cloudgaming.model.ServerResources;

import org.junit.Test;

public class TestServerResourcesDao extends TestHibernate {
	ServerResourcesDao serverResourcesDao;

	@Override
	public void setUp() {
		super.setUp();
		serverResourcesDao = new ServerResourcesDao();
	}
	
	/** Test for CRUD operations. */
	@Test
	public void generalTest() {
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		
		ServerResources entity = new ServerResources();
		entity.setRam(8);
		entity.setCpu(8);
		entity.setStorage(10);
		entity.setOs(OS.WINDOWS10);
		serverResourcesDao.save(entity);
		
		ServerResources entity2 = new ServerResources();
		entity2.setRam(16);
		entity2.setCpu(8);
		entity2.setStorage(25);
		entity2.setOs(OS.WINDOWS10);
		serverResourcesDao.save(entity2);
		
		assertEquals(10, serverResourcesDao.findByID(entity.getId_serverResources()).getStorage());
		assertEquals(25, serverResourcesDao.findByID(entity2.getId_serverResources()).getStorage());
		
		entity.setStorage(50);
		serverResourcesDao.update(entity);
		
		assertEquals(50, serverResourcesDao.findByID(entity.getId_serverResources()).getStorage());
		
		assertEquals(2, serverResourcesDao.findAll().size());
		
		serverResourcesDao.delete(entity);
		
		assertNull(serverResourcesDao.findByID(entity.getId_serverResources()));
		
		serverResourcesDao.deleteAll();
		
		assertEquals(0, serverResourcesDao.findAll().size());
		
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
	}
}
