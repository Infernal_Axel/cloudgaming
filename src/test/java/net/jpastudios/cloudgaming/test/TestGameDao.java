package net.jpastudios.cloudgaming.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import net.jpastudios.cloudgaming.GameGenre;
import net.jpastudios.cloudgaming.OS;
import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.dao.CompanyDao;
import net.jpastudios.cloudgaming.dao.GameDao;
import net.jpastudios.cloudgaming.dao.ServerResourcesDao;
import net.jpastudios.cloudgaming.model.Company;
import net.jpastudios.cloudgaming.model.Game;
import net.jpastudios.cloudgaming.model.ServerResources;

import org.junit.Test;

public class TestGameDao extends TestHibernate {
	GameDao gameDao;
	ServerResourcesDao serverResourcesDao;
	CompanyDao companyDao;

	@Override
	public void setUp() {
		super.setUp();
		gameDao = new GameDao();
		serverResourcesDao = new ServerResourcesDao();
		companyDao = new CompanyDao();
	}
	
	/** Test for CRUD operations. */
	@Test
	public void generalTest() {
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		
		ServerResources cs = new ServerResources();
		cs.setRam(8);
		cs.setCpu(8);
		cs.setStorage(10);
		cs.setOs(OS.WINDOWS10);
		serverResourcesDao.save(cs);
		
		ServerResources cs2 = new ServerResources();
		cs2.setRam(16);
		cs2.setCpu(8);
		cs2.setStorage(25);
		cs2.setOs(OS.WINDOWS10);
		serverResourcesDao.save(cs2);
		
		Company cp = new Company();
		cp.setName("test0");
		companyDao.save(cp);
		
		Company cp2 = new Company();
		cp2.setName("test1");
		companyDao.save(cp2);	
		
		Game game = new Game();
		game.setName("game0");
		game.setGenre(GameGenre.ACTION);
		game.setMinAge(18);
		game.setCompany(cp);
		game.setServerResources(cs);
		gameDao.save(game);
		
		Game game2 = new Game();
		game2.setName("game1");
		game2.setGenre(GameGenre.ACTION);
		game2.setMinAge(12);
		game2.setCompany(cp2);
		game2.setServerResources(cs2);
		gameDao.save(game2);
		
		assertEquals("game0", gameDao.findByID(game.getId_game()).getName());
		assertEquals("game1", gameDao.findByID(game2.getId_game()).getName());
		
		assertEquals(cp.getId_company(), gameDao.findByID(game.getId_game()).getCompany().getId_company());
		
		game.setGenre(GameGenre.HORROR);
		gameDao.update(game);
		
		assertEquals(GameGenre.HORROR, gameDao.findByID(game.getId_game()).getGenre());
		
		assertEquals(2, gameDao.findAll().size());
		
		gameDao.delete(game);
		
		assertNull(gameDao.findByID(game.getId_game()));
		
		gameDao.deleteAll();
		
		assertEquals(0, gameDao.findAll().size());
		
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
	}
}
