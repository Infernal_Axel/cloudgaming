package net.jpastudios.cloudgaming.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import net.jpastudios.cloudgaming.GameGenre;
import net.jpastudios.cloudgaming.OS;
import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.dao.CompanyDao;
import net.jpastudios.cloudgaming.dao.GameDao;
import net.jpastudios.cloudgaming.dao.PlaylistDao;
import net.jpastudios.cloudgaming.dao.ServerResourcesDao;
import net.jpastudios.cloudgaming.dao.UserDao;
import net.jpastudios.cloudgaming.model.Company;
import net.jpastudios.cloudgaming.model.Game;
import net.jpastudios.cloudgaming.model.Playlist;
import net.jpastudios.cloudgaming.model.ServerResources;
import net.jpastudios.cloudgaming.model.User;

import org.hibernate.LazyInitializationException;
import org.junit.Test;

public class TestPlaylistDao extends TestHibernate {
	PlaylistDao playlistDao;
	UserDao userDao;
	GameDao gameDao;
	ServerResourcesDao serverResourcesDao;
	CompanyDao companyDao;

	@Override
	public void setUp() {
		super.setUp();
		playlistDao = new PlaylistDao();
		userDao = new UserDao();
		gameDao = new GameDao();
		serverResourcesDao = new ServerResourcesDao();
		companyDao = new CompanyDao();
	}
	
	/** Test for CRUD operations. */
	@Test
	public void generalTest() {
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		
		ServerResources cs = new ServerResources();
		cs.setRam(8);
		cs.setCpu(8);
		cs.setStorage(10);
		cs.setOs(OS.WINDOWS10);
		serverResourcesDao.save(cs);
		
		Company cp = new Company();
		cp.setName("test0");
		companyDao.save(cp);	
		
		Game game = new Game();
		game.setName("game0");
		game.setGenre(GameGenre.ACTION);
		game.setMinAge(18);
		game.setCompany(cp);
		game.setServerResources(cs);
		gameDao.save(game);
		
		Game game2 = new Game();
		game2.setName("game1");
		game2.setGenre(GameGenre.ACTION);
		game2.setMinAge(18);
		game2.setCompany(cp);
		game2.setServerResources(cs);
		gameDao.save(game2);
		
		User user = new User();
		user.setUsername("giocatore0");
		user.setEmail("test@test.com");
		user.setDob(LocalDate.of(1990, 2, 13));
		user.setCurrentGame(null);
		userDao.save(user);
		
		Playlist playlist = new Playlist();
		playlist.setName("Favourites");
		playlist.addGame(game);
		user.addPlaylist(playlist);
		playlistDao.save(playlist);
					
		Playlist playlist2 = new Playlist();
		playlist2.setName("Done Games");
		playlist2.addGame(game);
		playlist2.addGame(game2);
		user.addPlaylist(playlist2);
		playlist.addSubPlaylist(playlist2);
		playlistDao.save(playlist2);
		
		assertEquals(user.getId_user(), playlistDao.findByID(playlist.getId_playlist()).getOwner().getId_user());
		
		assertEquals("Favourites", playlistDao.findByID(playlist.getId_playlist()).getName());
		assertEquals("game0", playlistDao.findByID(playlist.getId_playlist()).getGames().get(0).getName());
		
		assertEquals(2, userDao.findByID(user.getId_user()).getPlaylists().size());
		assertEquals(1, playlistDao.findByID(playlist.getId_playlist()).getSubPlaylists().size());
		assertEquals("Favourites", playlistDao.findByID(playlist2.getId_playlist()).getParentList().getName());
		
		Playlist playlist3 = new Playlist();
		playlist3.setName("Worst");
		playlist3.addGame(game2);
		user.addPlaylist(playlist3);
		playlistDao.save(playlist3);
		
		assertEquals(1, playlistDao.getSubPlaylists(playlist.getId_playlist()).size());
		assertEquals(2, playlistDao.findWithGame(game2).size());
		assertEquals(2, playlistDao.findLevel0WithUser(user).size());
		
		playlist.addSubPlaylist(playlist3);
		playlistDao.update(playlist3);
		
		assertEquals(2, playlistDao.getSubPlaylists(playlist.getId_playlist()).size());
		assertEquals(1, playlistDao.findLevel0WithUser(user).size());
		
		assertEquals(2, playlistDao.findByID(playlist.getId_playlist()).getSubPlaylists().size());
		
		assertEquals(3, playlistDao.findAll().size());
		
		playlistDao.delete(playlist);
		
		assertEquals(0, playlistDao.findAll().size()); //As it deletes children too!

		assertNull(playlistDao.findByID(playlist3.getId_playlist()));
				
		playlist = new Playlist();
		playlist.setName("Favourites");
		playlist.setParentList(null);
		playlist.addGame(game);
		user.addPlaylist(playlist);
		playlistDao.save(playlist);
		
		playlist2 = new Playlist();
		playlist2.setName("Done Games");
		playlist2.addGame(game);
		playlist2.addGame(game2);
		user.addPlaylist(playlist2);
		playlist.addSubPlaylist(playlist2);
		playlistDao.save(playlist2);
		
		assertEquals(2, playlistDao.findAll().size());
			
		playlistDao.deleteAll();
		
		assertEquals(0, playlistDao.findAll().size());
		
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
	}
	
	/** Testing Playlist lazy loading. */
	@Test(expected = LazyInitializationException.class)
	public void lazyLoadingTest() {
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		
		ServerResources cs = new ServerResources();
		cs.setRam(8);
		cs.setCpu(8);
		cs.setStorage(10);
		cs.setOs(OS.WINDOWS10);
		serverResourcesDao.save(cs);
		
		Company cp = new Company();
		cp.setName("test0");
		companyDao.save(cp);	
		
		Game game = new Game();
		game.setName("game0");
		game.setGenre(GameGenre.ACTION);
		game.setMinAge(18);
		game.setCompany(cp);
		game.setServerResources(cs);
		gameDao.save(game);
		
		Game game2 = new Game();
		game2.setName("game1");
		game2.setGenre(GameGenre.ACTION);
		game2.setMinAge(18);
		game2.setCompany(cp);
		game2.setServerResources(cs);
		gameDao.save(game2);
		
		User user = new User();
		user.setUsername("giocatore0");
		user.setEmail("test@test.com");
		user.setDob(LocalDate.of(1990, 2, 13));
		user.setCurrentGame(null);
		userDao.save(user);
		
		Playlist playlist = new Playlist();
		playlist.setName("Favourites");
		playlist.addGame(game);
		user.addPlaylist(playlist);
		playlistDao.save(playlist);
		
		Playlist playlist2 = new Playlist();
		playlist2.setName("Done Games");
		playlist2.addGame(game);
		playlist2.addGame(game2);
		user.addPlaylist(playlist2);
		playlist.addSubPlaylist(playlist2);
		playlistDao.save(playlist2);
		
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
		
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		Playlist pTest = playlistDao.findByID(playlist.getId_playlist());
		pTest.getSubPlaylists().size(); //Inizialize lazy field as Hibernate.inizialize() is bugged!
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
		assertEquals(1, pTest.getSubPlaylists().size());
		
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		Playlist pTest2 = playlistDao.findByID(playlist.getId_playlist());
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
		assertEquals(1, pTest2.getSubPlaylists().size()); //This must throw the Exception
	}
}
