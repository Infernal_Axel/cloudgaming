package net.jpastudios.cloudgaming.test;

import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.service.CloudGamingService;

import org.junit.After;
import org.junit.Before;

/** Abstract class to simplify the test classes. */
public abstract class TestHibernate {
	@Before
	public void setUp() {
		SessionManager.getInstance().loadTestingMode();
	}
	
	@After
	public void cleanDB()
	{
		CloudGamingService.getInstance().deleteEverything();
		SessionManager.getInstance().exit();
	}
}
