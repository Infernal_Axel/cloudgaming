package net.jpastudios.cloudgaming.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import net.jpastudios.cloudgaming.GameGenre;
import net.jpastudios.cloudgaming.OS;
import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.dao.CompanyDao;
import net.jpastudios.cloudgaming.dao.GameDao;
import net.jpastudios.cloudgaming.dao.PlaylistDao;
import net.jpastudios.cloudgaming.dao.ServerResourcesDao;
import net.jpastudios.cloudgaming.dao.UserDao;
import net.jpastudios.cloudgaming.model.Company;
import net.jpastudios.cloudgaming.model.Game;
import net.jpastudios.cloudgaming.model.Playlist;
import net.jpastudios.cloudgaming.model.ServerResources;
import net.jpastudios.cloudgaming.model.User;

import org.hibernate.LazyInitializationException;
import org.junit.Test;

public class TestUserDao extends TestHibernate {
	PlaylistDao playlistDao;
	UserDao userDao;
	GameDao gameDao;
	ServerResourcesDao serverResourcesDao;
	CompanyDao companyDao;

	@Override
	public void setUp() {
		super.setUp();
		playlistDao = new PlaylistDao();
		userDao = new UserDao();
		gameDao = new GameDao();
		serverResourcesDao = new ServerResourcesDao();
		companyDao = new CompanyDao();
	}
	
	/** Test for CRUD operations. */
	@Test
	public void generalTest() {
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		
		ServerResources cs = new ServerResources();
		cs.setRam(8);
		cs.setCpu(8);
		cs.setStorage(10);
		cs.setOs(OS.WINDOWS10);
		serverResourcesDao.save(cs);
		
		Company cp = new Company();
		cp.setName("test0");
		companyDao.save(cp);	
		
		Game game = new Game();
		game.setName("game0");
		game.setGenre(GameGenre.ACTION);
		game.setMinAge(18);
		game.setCompany(cp);
		game.setServerResources(cs);
		gameDao.save(game);
		
		User user = new User();
		user.setUsername("giocatore0");
		user.setEmail("test@test.com");
		user.setDob(LocalDate.of(1990, 2, 13));
		user.setCurrentGame(null);
		userDao.save(user);
		
		User user2 = new User();
		user2.setUsername("giocatore1");
		user2.setEmail("test2@test.com");
		user2.setDob(LocalDate.of(1993, 4, 4));
		user2.setCurrentGame(null);
		userDao.save(user2);
		
		assertEquals("test@test.com", userDao.findByID(user.getId_user()).getEmail());
		assertEquals("test2@test.com", userDao.findByID(user2.getId_user()).getEmail());
		
		user.setCurrentGame(game);
		userDao.update(user);
		
		assertEquals("game0", userDao.findByID(user.getId_user()).getCurrentGame().getName());
		
		assertEquals(2, userDao.findAll().size());
		
		userDao.delete(user);
		
		assertNull(userDao.findByID(user.getId_user()));
		
		userDao.deleteAll();
		
		assertEquals(0, userDao.findAll().size());
		
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
	}
	
	/** Testing User lazy loading. */
	@Test(expected = LazyInitializationException.class)
	public void lazyLoadingTest() {
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		
		ServerResources cs = new ServerResources();
		cs.setRam(8);
		cs.setCpu(8);
		cs.setStorage(10);
		cs.setOs(OS.WINDOWS10);
		serverResourcesDao.save(cs);
		
		Company cp = new Company();
		cp.setName("test0");
		companyDao.save(cp);	
		
		Game game = new Game();
		game.setName("game0");
		game.setGenre(GameGenre.ACTION);
		game.setMinAge(18);
		game.setCompany(cp);
		game.setServerResources(cs);
		gameDao.save(game);
		
		User user = new User();
		user.setUsername("giocatore0");
		user.setEmail("test@test.com");
		user.setDob(LocalDate.of(1990, 2, 13));
		user.setCurrentGame(null);
		userDao.save(user);
		
		Playlist playlist = new Playlist();
		playlist.setName("Favourites");
		playlist.addGame(game);
		user.addPlaylist(playlist);
		playlistDao.save(playlist);
		
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
		
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		User user2 = userDao.findByID(user.getId_user());
		user2.getPlaylists().size(); //Inizialize lazy field as Hibernate.inizialize() is bugged!
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
		assertEquals(1, user2.getPlaylists().size());
		
		SessionManager.getInstance().openCurrentSessionWithTransaction();
		User user3 = userDao.findByID(user.getId_user());
		SessionManager.getInstance().closeCurrentSessionWithTransaction();
		assertEquals(1, user3.getPlaylists().size()); //This must throw the Exception
	}
}
