package net.jpastudios.cloudgaming.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import net.jpastudios.cloudgaming.GameGenre;
import net.jpastudios.cloudgaming.OS;
import net.jpastudios.cloudgaming.model.Company;
import net.jpastudios.cloudgaming.model.Game;
import net.jpastudios.cloudgaming.model.Playlist;
import net.jpastudios.cloudgaming.model.ServerResources;
import net.jpastudios.cloudgaming.model.User;
import net.jpastudios.cloudgaming.service.CloudGamingService;

import org.junit.Test;

public class TestCloudGamingService extends TestHibernate {	
	private static long MAXVALUE = Long.MAX_VALUE;
	CloudGamingService service = CloudGamingService.getInstance();
	
	/** Test to check all normal behaviours of all the CloudGamingService functionalities. */
	@Test
	public void serviceFunctionalitiesTest()
	{
	    Company company0 = service.createCompany("Epic Games");
	    Company company1 = service.createCompany("Dice");
	    ServerResources serverResources0 = service.createServerResources(8, 8, 10, OS.WINDOWS10);
	    ServerResources serverResources1 = service.createServerResources(16, 8, 25, OS.WINDOWS10);
	    Game game0 = service.createGame("Fortnite", GameGenre.BATTLE_ROYAL, 12, company0, serverResources0);
	    Game game1 = service.createGame("Battlefront 2", GameGenre.SHOOTER, 16, company1, serverResources1);
	    Game game2 = service.createGame("Battlefield", GameGenre.SHOOTER, 18, company1, serverResources1);
	    User user0 = service.createUser("Axel", "test@yolo.it", LocalDate.of(2005, 2, 13));
	    User user1 = service.createUser("Matteo", "sample@yolo.it", LocalDate.of(1993, 4, 3));
	    ArrayList<Game> p0 = new ArrayList<Game>();
	    p0.add(game0);
	    p0.add(game1);
	    ArrayList<Game> p1 = new ArrayList<Game>();
	    p1.add(game1);
	    p1.add(game2);
	    Playlist playlist0 = service.createPlaylist(user0, "games belli", p0, null);
	    Playlist playlist1 = service.createPlaylist(user0, "games tosti", p1, playlist0);
	    
	    assertEquals("Matteo", service.findUser(user1.getId_user()).getUsername());
	    assertEquals("Fortnite", service.findGame(game0.getId_game()).getName());
	    assertEquals("Dice", service.findCompany(company1.getId_company()).getName());
	    assertEquals(8, service.findServerResources(serverResources0.getId_serverResources()).getRam());
	    
	    assertNotNull(service.findPlaylist(playlist0.getId_playlist()));
	    
	    service.addGameInPlaylist(playlist0.getId_playlist(), game2.getId_game());
	    
	    assertNotNull(service.findPlaylist(playlist0.getId_playlist()));
	    assertNotNull(service.findPlaylist(playlist1.getId_playlist()));
	    assertEquals(3, service.findPlaylist(playlist0.getId_playlist()).getGames().size());
	    assertNull(service.findSubPlaylists(MAXVALUE));
	    assertEquals("games tosti", service.findSubPlaylists(playlist0.getId_playlist()).get(0).getName());
	    
	    service.updateUserCurrentGame(user0.getId_user(), game1.getId_game());
	    service.updateUserCurrentGame(user1.getId_user(), game2.getId_game());
	    
	    assertEquals(game1.getId_game(), service.findUser(user0.getId_user()).getCurrentGame().getId_game());
	    
	    service.updateUserInfo(user1.getId_user(), "Donald", "goofy@goofy.com", LocalDate.of(1993, 4, 3));
	    
	    assertEquals("Donald", service.findUser(user1.getId_user()).getUsername());
	    
	    service.updateGame(game0.getId_game(), game0.getName(), game0.getGenre(), 10, game0.getCompany(), game0.getServerResources());

	    assertEquals(10, service.findGame(game0.getId_game()).getMinAge());
	    
	    service.updatePlaylist(playlist0.getId_playlist(), "Amazing games", null);
	    
	    assertEquals("Amazing games", service.findPlaylist(playlist0.getId_playlist()).getName());

	    assertEquals(2, service.findAllUsers().size());
	    assertEquals(3, service.findAllAvailableGames().size());

	    List<User> uList = service.findUsersThatHaveCurrentGame(game2.getId_game());
	    List<Playlist> pList = service.findPlaylistsThatHaveGame(game1.getId_game());
	    
	    assertEquals(1, uList.size());
	    assertEquals(2, pList.size());

	    assertTrue(service.canUserPlayGame(service.findUser(user0.getId_user()), service.findGame(game0.getId_game())));
	    assertFalse(service.canUserPlayGame(service.findUser(user0.getId_user()), service.findGame(game2.getId_game())));
	    assertFalse(service.findUserLevel0Playlists(user0.getId_user()).isEmpty());
	    
	    service.setSameCurrentGameOfOtherUser(user0.getId_user(), user1.getId_user());
	    assertEquals("Battlefront 2", service.findUser(user0.getId_user()).getCurrentGame().getName());
	    
	    service.removeGameFromPlaylist(playlist0.getId_playlist(), game0.getId_game());
	    
	    assertEquals(2, service.findPlaylist(playlist0.getId_playlist()).getGames().size());
	    
	    service.deletePlaylistAndChildren(playlist0.getId_playlist());
	    
	    assertNull(service.findPlaylist(playlist0.getId_playlist()));
	    
	    service.deleteUserAndHisPlaylists(user1.getId_user());
	    
	    assertNull(service.findPlaylist(playlist1.getId_playlist()));
	    
	    assertEquals(0, service.findUserIncludingPlaylists(user0.getId_user()).getPlaylists().size());
	    
	    service.deleteGame(game0.getId_game());
	    
	    assertNull(service.findGame(game0.getId_game()));
	    
	    service.deleteUserAndHisPlaylists(user0.getId_user());
	    
	    assertNull(service.findUser(user0.getId_user()));
	    
	    service.deleteServerResources(serverResources0.getId_serverResources());
	    
	    assertNull(service.findServerResources(serverResources0.getId_serverResources()));
	    
	    service.deleteCompany(company0.getId_company());
	    
	    assertNull(service.findCompany(company0.getId_company()));
	    
	    service.deleteEverything();
	    
	    assertEquals(0, service.findAllUsers().size());
	    assertEquals(0, service.findAllAvailableGames().size());
	}
	
	/** Checks of null values in the service methods. No transaction should be executed. */
	
	@Test(expected = IllegalArgumentException.class)
	public void createGameNullTest() {
		service.createGame("", null, 1, null, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void createServerResourcesNullTest() {
		service.createServerResources(0, 0, 0, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void createPlaylistNullTest() {
		service.createPlaylist(null, "", null, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void updateUserCurrentGameNullTest() {
		service.updateUserCurrentGame(MAXVALUE, MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void updateCompanyNullTest() {
		service.updateCompany(MAXVALUE, "");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void updateGameNullTest() {
		service.updateGame(MAXVALUE, "", null, 0, null, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void updatePlaylistNullTest() {
		service.updatePlaylist(MAXVALUE, "", null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void updateServerResourcesNullTest() {
		service.updateServerResources(MAXVALUE, 0, 0, 0, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addGameInPlaylistNullTest() {
		service.addGameInPlaylist(MAXVALUE, MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void removeGameFromPlaylistNullTest() {
		service.removeGameFromPlaylist(MAXVALUE, MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void deleteUserAndHisPlaylistsNullTest() {
		service.deleteUserAndHisPlaylists(MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void deleteGameNullTest() {
		service.deleteGame(MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void deleteCompanyNullTest() {
		service.deleteCompany(MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void deleteServerResourcesNullTest() {
		service.deleteServerResources(MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void deletePlaylistAndChildrenNullTest() {
		service.deletePlaylistAndChildren(MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void findPlaylistsThatHaveGameNullTest() {
		service.findPlaylistsThatHaveGame(MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void findUsersThatHaveCurrentGameNullTest() {
		service.findUsersThatHaveCurrentGame(MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void findUserLevel0PlaylistsNullTest() {
		service.findUserLevel0Playlists(MAXVALUE);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void canUserPlayGame() {
		service.canUserPlayGame(null, null);
	}
}
