package net.jpastudios.cloudgaming;

public enum GameGenre {
	ACTION, RPG, BATTLE_ROYAL, HORROR, FIGHTING, SHOOTER, PUZZLE;
}
