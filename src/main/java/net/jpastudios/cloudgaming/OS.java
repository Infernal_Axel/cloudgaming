package net.jpastudios.cloudgaming;

public enum OS {
	WINDOWS10, WINDOWS8, WINDOWSXP, MACOSX, IOS, ANDROID, PS4, PS3, PS2, XBOX_ONE, WII, SWITCH, NINTENDO_DS;
}
