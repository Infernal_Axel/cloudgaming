package net.jpastudios.cloudgaming;

import java.util.List;

import net.jpastudios.cloudgaming.model.Game;
import net.jpastudios.cloudgaming.model.User;
import net.jpastudios.cloudgaming.service.CloudGamingService;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/** This class shouldn't be the entrypoint for using the program: 
 * it should rather be compiled and used as library by a bigger system 
 * or a proper GUI should be developed for using the service methods.
 * If really needed, line commands can be created and used without a GUI, 
 * as showed by these examples.
 * @author Matteo Coppola
 */
public class Main {
	/** Using CLI we show here 2 line command examples. You can call the program via Java passing
	 * "f_users" or "f_games" as parameters.
	 */
	public static void main(String [] args)
	{		
        Options options = new Options();

        Option f_users = new Option("f_users", "find users", false, "find all users");
        options.addOption(f_users);

        Option f_games = new Option("f_games", "find games", false, "find all games");
        options.addOption(f_games);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
            
            if(cmd.hasOption("f_users")) {
            	List<User> l = CloudGamingService.getInstance().findAllUsers();
            	for(User u : l) {
            		System.out.println("User: "+u.getId_user()+" "+u.getEmail()+" "+u.getUsername()+" "+u.getDob());
            	}
            } else if(cmd.hasOption("f_games")) {
            	List<Game> l = CloudGamingService.getInstance().findAllAvailableGames();
            	for(Game g : l) {
            		System.out.println("Game: "+g.getId_game()+" "+g.getName()+" "+g.getMinAge());
            	}
            } else {
                System.out.println("You need to specify a command");
            }
            
            SessionManager.getInstance().exit();
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
        }
	}
}
