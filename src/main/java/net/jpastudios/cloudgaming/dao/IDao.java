package net.jpastudios.cloudgaming.dao;

import java.io.Serializable;
import java.util.List;

/** Interface for DAO classes that implement CRUD operations. */
public interface IDao<T, Id extends Serializable> {
	public void save(T entity);
	public void update(T entity);
	public T findByID(Id id);
	public void delete(T entity);
	public List<T> findAll();
	public void deleteAll();
}
