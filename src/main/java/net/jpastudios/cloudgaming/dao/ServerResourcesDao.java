package net.jpastudios.cloudgaming.dao;

import java.util.List;

import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.model.ServerResources;

public class ServerResourcesDao implements IDao<ServerResources, Long> {

	@Override
	public void save(ServerResources entity) {
		SessionManager.instance.getCurrentSession().save(entity);
	}

	@Override
	public void update(ServerResources entity) {
		SessionManager.instance.getCurrentSession().update(entity);
	}

	@Override
	public ServerResources findByID(Long id) {
		return SessionManager.instance.getCurrentSession().get(ServerResources.class, id);
	}

	@Override
	public void delete(ServerResources entity) {
		SessionManager.instance.getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ServerResources> findAll() {
		return (List<ServerResources>)SessionManager.instance.getCurrentSession().createQuery("from ServerResources").list();
	}

	@Override
	public void deleteAll() {
		List<ServerResources> l = findAll();
		for(ServerResources c : l) {
			delete(c);
		}
	}
}
