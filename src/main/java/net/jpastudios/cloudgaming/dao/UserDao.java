package net.jpastudios.cloudgaming.dao;

import java.util.List;

import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.model.Game;
import net.jpastudios.cloudgaming.model.User;

import org.hibernate.query.Query;

public class UserDao implements IDao<User, Long> {

	@Override
	public void save(User entity) {
		SessionManager.instance.getCurrentSession().save(entity);
	}

	@Override
	public void update(User entity) {
		SessionManager.instance.getCurrentSession().update(entity);
	}

	@Override
	public User findByID(Long id) {
		return SessionManager.instance.getCurrentSession().get(User.class, id);
	}
	
	/** Method to find the user loading also his playlists (normally they are not loaded). */
	public User findByIDIncludingPlaylists(Long id) {
		User u = SessionManager.instance.getCurrentSession().get(User.class, id);
		u.getPlaylists().size(); //Inizialize lazy field as Hibernate.initialize() is bugged!
		return u;
	}

	@Override
	public void delete(User entity) {
		SessionManager.instance.getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAll() {
		return (List<User>)SessionManager.instance.getCurrentSession().createQuery("from User").list();
	}

	@Override
	public void deleteAll() {
		List<User> l = findAll();
		for(User c : l) {
			delete(c);
		}
	}
	
	/** Find all users who have the same current game. */
	public List<User> findUsersWithCurrentGame(Game game) {
		String hql = "FROM User U WHERE U.currentGame = "+game.getId_game();
		Query query = SessionManager.getInstance().getCurrentSession().createQuery(hql);
		return query.list();
	}
}