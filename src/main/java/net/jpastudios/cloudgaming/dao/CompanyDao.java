package net.jpastudios.cloudgaming.dao;

import java.util.List;

import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.model.Company;

public class CompanyDao implements IDao<Company, Long> {

	@Override
	public void save(Company entity) {
		SessionManager.instance.getCurrentSession().save(entity);
	}

	@Override
	public void update(Company entity) {
		SessionManager.instance.getCurrentSession().update(entity);
	}

	@Override
	public Company findByID(Long id) {
		return SessionManager.instance.getCurrentSession().get(Company.class, id);
	}

	@Override
	public void delete(Company entity) {
		SessionManager.instance.getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Company> findAll() {
		return (List<Company>)SessionManager.instance.getCurrentSession().createQuery("from Company").list();
	}

	@Override
	public void deleteAll() {
		List<Company> l = findAll();
		for(Company c : l) {
			delete(c);
		}
	}
}
