package net.jpastudios.cloudgaming.dao;

import java.util.List;

import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.model.Game;
import net.jpastudios.cloudgaming.model.Playlist;
import net.jpastudios.cloudgaming.model.User;

import org.hibernate.query.Query;

public class PlaylistDao implements IDao<Playlist, Long> {

	@Override
	public void save(Playlist entity) {
		SessionManager.instance.getCurrentSession().save(entity);
	}

	@Override
	public void update(Playlist entity) {
		SessionManager.instance.getCurrentSession().update(entity);
	}

	@Override
	public Playlist findByID(Long id) {
		return SessionManager.instance.getCurrentSession().get(Playlist.class, id);
	}

	@Override
	public void delete(Playlist entity) {
		SessionManager.instance.getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Playlist> findAll() {
		return (List<Playlist>)SessionManager.instance.getCurrentSession().createQuery("from Playlist").list();
	}

	@Override
	public void deleteAll() {
		List<Playlist> l = findAll();
		for(Playlist c : l) {
			delete(c);
		}
	}

	/** Find playlists which contain the game. */
	public List<Playlist> findWithGame(Game game) {
		String hql = "FROM Playlist P JOIN P.games G WHERE G.id_game = "+game.getId_game();
		Query query = SessionManager.getInstance().getCurrentSession().createQuery(hql);
		return query.list();
	}

	/** Find level 0 playlists (which don't have a parent) of the user. */
	public List<Playlist> findLevel0WithUser(User user) {
		String hql = "FROM Playlist P WHERE P.owner = "+user.getId_user()+" AND P.parentList is null";
		Query query = SessionManager.getInstance().getCurrentSession().createQuery(hql);
		return query.list();
	}

	/** Get playlists contained by this playlist. Useful because of lazy loading. */
	public List<Playlist> getSubPlaylists(Long playlistID) {
		Playlist p = SessionManager.instance.getCurrentSession().get(Playlist.class, playlistID);
		if(p != null) {
			p.getSubPlaylists().size(); //Inizialize lazy field as Hibernate.initialize() is bugged!
			return p.getSubPlaylists();
		} else {
			return null;
		}
	}
}