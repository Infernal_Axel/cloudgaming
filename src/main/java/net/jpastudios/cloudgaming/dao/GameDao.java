package net.jpastudios.cloudgaming.dao;

import java.util.List;

import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.model.Game;

public class GameDao implements IDao<Game, Long> {

	@Override
	public void save(Game entity) {
		SessionManager.instance.getCurrentSession().save(entity);
	}

	@Override
	public void update(Game entity) {
		SessionManager.instance.getCurrentSession().update(entity);
	}

	@Override
	public Game findByID(Long id) {
		return SessionManager.instance.getCurrentSession().get(Game.class, id);
	}

	@Override
	public void delete(Game entity) {
		SessionManager.instance.getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Game> findAll() {
		return (List<Game>)SessionManager.instance.getCurrentSession().createQuery("from Game").list();
	}

	@Override
	public void deleteAll() {
		List<Game> l = findAll();
		for(Game c : l) {
			delete(c);
		}
	}
}