package net.jpastudios.cloudgaming.service;

import java.time.LocalDate;
import java.util.List;

import org.hibernate.HibernateException;

import net.jpastudios.cloudgaming.GameGenre;
import net.jpastudios.cloudgaming.OS;
import net.jpastudios.cloudgaming.SessionManager;
import net.jpastudios.cloudgaming.dao.CompanyDao;
import net.jpastudios.cloudgaming.dao.GameDao;
import net.jpastudios.cloudgaming.dao.PlaylistDao;
import net.jpastudios.cloudgaming.dao.ServerResourcesDao;
import net.jpastudios.cloudgaming.dao.UserDao;
import net.jpastudios.cloudgaming.model.Company;
import net.jpastudios.cloudgaming.model.Game;
import net.jpastudios.cloudgaming.model.Playlist;
import net.jpastudios.cloudgaming.model.ServerResources;
import net.jpastudios.cloudgaming.model.User;

/** Singleton class which offers all the public methods to manage the Cloud Gaming logic. */
public class CloudGamingService {
	public static final CloudGamingService instance = new CloudGamingService();

	private CompanyDao companyDao;
	private ServerResourcesDao serverResourcesDao;
	private GameDao gameDao;
	private PlaylistDao playlistDao;
	private UserDao userDao;

	private CloudGamingService() {
		companyDao = new CompanyDao();
		serverResourcesDao = new ServerResourcesDao();
		gameDao = new GameDao();
		playlistDao = new PlaylistDao();
		userDao = new UserDao();
	}

	/**
	 *  Create User.
	 * @param name
	 * @param email
	 * @param dob: date of birth.
	 */
	public User createUser(String name, String email, LocalDate dob) 
	{
		try {
			User user = new User();
			user.setUsername(name);
			user.setEmail(email);
			user.setDob(dob);
			user.setCurrentGame(null);

			SessionManager.getInstance().openCurrentSessionWithTransaction();	
			userDao.save(user);		

			return user;
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Create Game.
	 * @param name: game name.
	 * @param genre: game genre.
	 * @param minAge: minimum age to play this game. 
	 * @param company: Company that made this game, cannot be null.
	 * @param serverResources: Cloud resources for this game, cannot be null.
	 */
	public Game createGame(String name, GameGenre genre, int minAge, Company company, ServerResources serverResources)
	{
		if(company == null || serverResources == null) {
			throw new IllegalArgumentException("Cannot create Game. Need to specify Company and ServerResources.");
		}

		try {
			Game game = new Game();
			game.setName(name);
			game.setGenre(genre);
			game.setMinAge(minAge);
			game.setCompany(company);
			game.setServerResources(serverResources);

			SessionManager.getInstance().openCurrentSessionWithTransaction();	
			gameDao.save(game);		

			return game;
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** 
	 * Create game company.
	 * @param name: name of company.
	 */
	public Company createCompany(String name)
	{
		try{
			Company company = new Company();
			company.setName(name);

			SessionManager.getInstance().openCurrentSessionWithTransaction();	
			companyDao.save(company);

			return company;
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/**
	 * Create server resources setting.
	 * @param ram
	 * @param cpu
	 * @param storage
	 * @param os: Operating System, cannot be null.
	 */
	public ServerResources createServerResources(int ram, int cpu, int storage, OS os)
	{
		if(os == null) {
			throw new IllegalArgumentException("Cannot create ServerResources. Need to specify OS.");
		}
		
		try{
			ServerResources serverResources = new ServerResources();
			serverResources.setRam(ram);
			serverResources.setCpu(cpu);
			serverResources.setStorage(storage);
			serverResources.setOs(os);

			SessionManager.getInstance().openCurrentSessionWithTransaction();	
			serverResourcesDao.save(serverResources);

			return serverResources;
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Create a new playlist. 
	 * @param user: owner of the playlist, mustn't be null
	 * @param name: name of the playlist
	 * @param games: list of games in the playlist, can be empty or null
	 * @param parent: playlist that contains this one, null only if this playlist is not a subplaylist
	 */
	public Playlist createPlaylist(User user, String name, List<Game> games, Playlist parent)
	{
		if(user == null) {
			throw new IllegalArgumentException("Cannot create Playlist. Need to specify User.");
		}

		try {
			Playlist playlist = new Playlist();
			playlist.setOwner(user);
			playlist.setName(name);

			if(games != null) {
				for(Game g : games) {
					playlist.addGame(g);
				}
			}

			user.addPlaylist(playlist);

			if(parent != null)
				parent.addSubPlaylist(playlist);

			SessionManager.getInstance().openCurrentSessionWithTransaction();
			playlistDao.save(playlist);
			return playlist;
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Find an user and return it without his playlists loaded. */
	public User findUser(long id) {
		SessionManager.getInstance().openCurrentSession();
		User user = userDao.findByID(id);
		SessionManager.getInstance().closeCurrentSession();

		return user;
	}

	/** Find an user and return it with his playlists loaded. */
	public User findUserIncludingPlaylists(long id) {
		SessionManager.getInstance().openCurrentSession();
		User user = userDao.findByIDIncludingPlaylists(id); 
		SessionManager.getInstance().closeCurrentSession();

		return user;
	}

	/** Find game. */
	public Game findGame(long id) {
		SessionManager.getInstance().openCurrentSession();
		Game game = gameDao.findByID(id);
		SessionManager.getInstance().closeCurrentSession();

		return game;
	}

	/** Find company. */
	public Company findCompany(long id) {
		SessionManager.getInstance().openCurrentSession();
		Company company = companyDao.findByID(id);
		SessionManager.getInstance().closeCurrentSession();

		return company;
	}

	/** Find ServerResources. */
	public ServerResources findServerResources(long id) {
		SessionManager.getInstance().openCurrentSession();
		ServerResources serverResources = serverResourcesDao.findByID(id);
		SessionManager.getInstance().closeCurrentSession();

		return serverResources;
	}

	/** Find playlist and return it without its subplaylists loaded. */
	public Playlist findPlaylist(long id) {
		SessionManager.getInstance().openCurrentSession();
		Playlist playlist = playlistDao.findByID(id);
		SessionManager.getInstance().closeCurrentSession();

		return playlist;
	}

	/** Return playlist subPlaylists (load them separately because of lazy loading). */
	public List<Playlist> findSubPlaylists(long parentID) 
	{
		SessionManager.getInstance().openCurrentSession();
		List<Playlist> l = playlistDao.getSubPlaylists(parentID);
		SessionManager.getInstance().closeCurrentSession();

		return l;
	}

	/** Update User current game. */
	public void updateUserCurrentGame(long userID, long gameID){
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			User user = userDao.findByID(userID);
			Game game = gameDao.findByID(gameID);
			if(user == null) {
				throw new IllegalArgumentException("Cannot set current game. User doesn't exist.");
			} else {
				user.setCurrentGame(game);

				userDao.update(user);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Update user information. */
	public void updateUserInfo(long userID, String name, String email, LocalDate dob) 
	{
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			User user = userDao.findByID(userID);
			if(user == null) {
				throw new IllegalArgumentException("Cannot update. User doesn't exist.");
			} else {
				user.setUsername(name);
				user.setEmail(email);
				user.setDob(dob);

				userDao.update(user);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** 
	 * Update game.
	 * @param gameID
	 * @param name
	 * @param genre
	 * @param minAge
	 * @param company: cannot be null.
	 * @param serverResources: cannot be null.
	 */
	public void updateGame(long gameID, String name, GameGenre genre, int minAge, Company company, ServerResources serverResources)
	{
		if(company == null || serverResources == null) {
			throw new IllegalArgumentException("Cannot update Game. Need to specify Company and ServerResources.");
		}
		
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			Game game = gameDao.findByID(gameID);
			if(game == null) {
				throw new IllegalArgumentException("Cannot update. Game doesn't exist.");
			} else {
				game.setName(name);
				game.setGenre(genre);
				game.setMinAge(minAge);
				game.setCompany(company);
				game.setServerResources(serverResources);

				gameDao.update(game);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Update company. */
	public void updateCompany(long companyID, String name)
	{
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			Company company = companyDao.findByID(companyID);
			if(company == null) {
				throw new IllegalArgumentException("Cannot update. Company doesn't exist.");
			} else {
				company.setName(name);

				companyDao.update(company);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/**
	 * Update ServerResources.
	 * @param serverResourcesID
	 * @param memory
	 * @param cpu
	 * @param storage
	 * @param os: Operating System, cannot be null.
	 */
	public void updateServerResources(long serverResourcesID, int memory, int cpu, int storage, OS os)
	{
		if(os == null) {
			throw new IllegalArgumentException("Cannot update. Must specify Os.");
		}
		
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			ServerResources serverResources = serverResourcesDao.findByID(serverResourcesID);
			if(serverResources == null) {
				throw new IllegalArgumentException("Cannot update. ServerResources doesn't exist.");
			} else {
				serverResources.setRam(memory);
				serverResources.setCpu(cpu);
				serverResources.setStorage(storage);
				serverResources.setOs(os);

				serverResourcesDao.update(serverResources);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Update playlist name, eventually adding new games to the list. */
	public void updatePlaylist(long playlistID, String name, List<Game> gamesToAdd)
	{
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			Playlist playlist = playlistDao.findByID(playlistID);
			if(playlist == null) {
				throw new IllegalArgumentException("Cannot update. Playlist doesn't exist.");
			} else {
				playlist.setName(name);

				if(gamesToAdd != null)
					for(Game g : gamesToAdd) {
						playlist.addGame(g);
					}

				playlistDao.update(playlist);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Add a game to a playlist. */
	public void addGameInPlaylist(long playlistID, long gameID)
	{
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			Playlist playlist = playlistDao.findByID(playlistID);
			Game game = gameDao.findByID(gameID);
			if(playlist == null || game == null)
			{
				throw new IllegalArgumentException("Cannot add. Playlist or Game doesn't exist.");
			} else {
				playlist.addGame(game);

				playlistDao.update(playlist);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Remove a game from a playlist. */
	public void removeGameFromPlaylist(long playlistID, long gameID)
	{
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			Playlist playlist = playlistDao.findByID(playlistID);
			Game game = gameDao.findByID(gameID);
			if(playlist == null || game == null)
			{
				throw new IllegalArgumentException("Cannot remove. Playlist or Game doesn't exist.");
			} else {
				playlist.removeGame(game);

				playlistDao.update(playlist);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Deleting an user also means deleting all his playlists because of Hibernate cascade. */
	public void deleteUserAndHisPlaylists(long userID) {	
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			User user = userDao.findByID(userID);
			if(user == null)
			{
				throw new IllegalArgumentException("Cannot delete. User doesn't exist.");
			} else {
				userDao.delete(user);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Delete a game. */
	public void deleteGame(long gameID) {
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			Game game = gameDao.findByID(gameID);
			if(game == null)
			{
				throw new IllegalArgumentException("Cannot delete. Game doesn't exist.");
			} else {
				gameDao.delete(game);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Delete a Company. */
	public void deleteCompany(long companyID) {
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			Company company = companyDao.findByID(companyID);
			if(company == null)
			{
				throw new IllegalArgumentException("Cannot delete. Company doesn't exist.");
			} else {
				companyDao.delete(company);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Delete a ServerResources. */
	public void deleteServerResources(long serverResourcesID) {	 
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			ServerResources serverResources = serverResourcesDao.findByID(serverResourcesID);
			if(serverResources == null)
			{
				throw new IllegalArgumentException("Cannot delete. ServerResources doesn't exist.");
			} else {
				serverResourcesDao.delete(serverResources);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Deleting a playlist also means deleting all his children subPlaylists. */
	public void deletePlaylistAndChildren(long playlistID) {
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			Playlist playlist = playlistDao.findByID(playlistID);
			if(playlist == null)
			{
				throw new IllegalArgumentException("Cannot delete. Playlist doesn't exist.");
			} else {
				playlistDao.delete(playlist);
			}
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Find playlists that contain the game. */
	@SuppressWarnings("unchecked")
	public List<Playlist> findPlaylistsThatHaveGame(long gameID)
	{
		SessionManager.getInstance().openCurrentSession();
		Game game = gameDao.findByID(gameID);
		if(game == null)
			throw new IllegalArgumentException("Cannot find. Game does not exist.");
		List<Playlist> list = playlistDao.findWithGame(game);
		SessionManager.getInstance().closeCurrentSession();
		return list;
	}

	/** Find users that are currently playing the game. */
	@SuppressWarnings("unchecked")
	public List<User> findUsersThatHaveCurrentGame(long gameID)
	{
		SessionManager.getInstance().openCurrentSession();
		Game game = gameDao.findByID(gameID);
		if(game == null)
			throw new IllegalArgumentException("Cannot find. Game does not exist.");
		List<User> list = userDao.findUsersWithCurrentGame(game);
		SessionManager.getInstance().closeCurrentSession();
		return list;
	}

	/** Find all main playlists of the user ("level 0" means they are not subPlaylists of other lists).
	 * This method is necessary because the user instance contains all playlists owned by the user, not
	 * only the "level 0" ones. */
	@SuppressWarnings("unchecked")
	public List<Playlist> findUserLevel0Playlists(long userID) {
		SessionManager.getInstance().openCurrentSession();
		User user = userDao.findByID(userID);
		if(user == null)
			throw new IllegalArgumentException("Cannot find. User does not exist.");
		List<Playlist> list = playlistDao.findLevel0WithUser(user);
		SessionManager.getInstance().closeCurrentSession();
		return list;
	}

	/** Find all user. */
	public List<User> findAllUsers() {
		SessionManager.getInstance().openCurrentSession();
		List<User> res = userDao.findAll();
		SessionManager.getInstance().closeCurrentSession();
		return res;
	}

	/** Find all available games managed by the cloud system. */
	public List<Game> findAllAvailableGames() {
		SessionManager.getInstance().openCurrentSession();
		List<Game> res = gameDao.findAll();
		SessionManager.getInstance().closeCurrentSession();
		return res;
	}

	/** Set same current game. */
	public void setSameCurrentGameOfOtherUser(long userID, long otherUserID)
	{
		SessionManager.getInstance().openCurrentSession();
		User user = userDao.findByID(userID);
		Game game = user.getCurrentGame();
		SessionManager.getInstance().closeCurrentSession();
		this.updateUserCurrentGame(otherUserID, game.getId_game());
	}

	/** Delete everything from the Database. */
	public void deleteEverything()
	{
		try {
			SessionManager.getInstance().openCurrentSessionWithTransaction();
			userDao.deleteAll();
			playlistDao.deleteAll();
			gameDao.deleteAll();
			companyDao.deleteAll();
			serverResourcesDao.deleteAll();
		}catch(HibernateException e){
			if (SessionManager.instance.getCurrentTransaction() != null) {
				SessionManager.instance.getCurrentTransaction().rollback();   

			}
			throw e;
		}finally {
			if(SessionManager.getInstance().getCurrentSession() != null)
				SessionManager.getInstance().closeCurrentSessionWithTransaction();
		}
	}

	/** Check if user can play the game based on his age. */
	public boolean canUserPlayGame(User user, Game game) {
		if(user == null || game == null)
		{
			throw new IllegalArgumentException("Cannot check. User or Game is null.");
		}
		return LocalDate.now().getYear() - user.getDob().getYear() >= game.getMinAge();
	}

	/** Singleton instance of this class. */
	public static CloudGamingService getInstance() {
		return instance;
	}
}
