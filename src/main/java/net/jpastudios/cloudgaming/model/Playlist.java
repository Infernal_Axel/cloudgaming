package net.jpastudios.cloudgaming.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/** Entity representing a user playlist. It can contain games and other playlists. */
@Entity
@Table(name = "playlist")
public class Playlist {
	@Id
	@Column(name = "id_playlist")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id_playlist;
	private String name;
	
	/** Playlist which contains this one. */
	@ManyToOne(optional = true)
	@JoinColumn(name = "parent_list", referencedColumnName = "id_playlist", nullable=true)
	private Playlist parentList;
	
	/** Playlists contained by this one. */
	@OneToMany(mappedBy = "parentList", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, orphanRemoval=true)
	private List<Playlist> subPlaylists;
	
	/** Games listed in this playlist. */
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "playlist_game", 
        joinColumns = { @JoinColumn(name = "id_playlist") }, 
        inverseJoinColumns = { @JoinColumn(name = "id_game") }
    )	
	private List<Game> games;
	
	@ManyToOne(optional = false)
    @JoinColumn(name="id_user", nullable=false)
	private User owner;

	public Playlist() {
		subPlaylists = new ArrayList<Playlist>();
		games = new ArrayList<Game>();
	}
	
	public long getId_playlist() {
		return id_playlist;
	}

	public void setId_playlist(long id_playlist) {
		this.id_playlist = id_playlist;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Playlist> getSubPlaylists() {
		return (List<Playlist>) Collections.unmodifiableList(subPlaylists);
	}
	
	public void addSubPlaylist(Playlist subPlaylist) {
		subPlaylist.setParentList(this);
		this.subPlaylists.add(subPlaylist);
	}
	
	public void removeSubPlaylist(Playlist subPlaylist) {
		this.subPlaylists.remove(subPlaylist);
	}

	public List<Game> getGames() {
		return (List<Game>) Collections.unmodifiableList(games);
	}
	
	public void addGame(Game game) {
		this.games.add(game);
	}
	
	public void removeGame(Game game) {
		this.games.remove(game);
	}
	
	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Playlist getParentList() {
		return parentList;
	}

	public void setParentList(Playlist parentList) {
		this.parentList = parentList;
	}
}
