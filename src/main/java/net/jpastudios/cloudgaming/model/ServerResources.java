package net.jpastudios.cloudgaming.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import net.jpastudios.cloudgaming.OS;

/** Entity representing the resources needed for running a game. */
@Entity
@Table(name = "serverresources")
public class ServerResources {
	@Id
	@Column(name = "id_cloudserver")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id_serverResources;
	private int ram;
	private int cpu;
	private int storage;
	
	/** Operating System that can run the game. */
	@Enumerated
    @Column(columnDefinition = "smallint")
	private OS os;
	
	public ServerResources() {
	}

	public long getId_serverResources() {
		return id_serverResources;
	}

	public void setId_serverResources(long id_serverResources) {
		this.id_serverResources = id_serverResources;
	}
	
	public int getRam() {
		return ram;
	}

	public void setRam(int ram) {
		this.ram = ram;
	}

	public int getCpu() {
		return cpu;
	}
	
	public void setCpu(int cpu) {
		this.cpu = cpu;
	}
	
	public int getStorage() {
		return storage;
	}
	
	public void setStorage(int storage) {
		this.storage = storage;
	}
	
	public OS getOs() {
		return os;
	}
	
	public void setOs(OS os) {
		this.os = os;
	}
}
