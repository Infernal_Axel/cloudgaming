package net.jpastudios.cloudgaming.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import net.jpastudios.cloudgaming.GameGenre;

/** Entity representing a videogame. */
@Entity
@Table(name = "game")
public class Game {
	@Id
	@Column(name = "id_game")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id_game;
	private String name;
	
	/** Type of game. */
	@Enumerated
    @Column(columnDefinition = "smallint")
	private GameGenre genre;
	
	/** Minimum age to play this game. */
	private int minAge;
	
	/** Company which created this game. */
	@ManyToOne(optional = false)
    @JoinColumn(name="id_company", nullable=false)
	private Company company;
	
	/** Cloud resources needed for launching and running this game. 
	 * A bigger system can consult these instances to allocate better the resource among the servers. */
	@ManyToOne(optional = false)
    @JoinColumn(name="id_serverResources", nullable=false)
	private ServerResources serverResources;

	public Game() {
	}

	public long getId_game() {
		return id_game;
	}

	public void setId_game(long id_game) {
		this.id_game = id_game;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GameGenre getGenre() {
		return genre;
	}

	public void setGenre(GameGenre genre) {
		this.genre = genre;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public ServerResources getServerResources() {
		return serverResources;
	}

	public void setServerResources(ServerResources serverResources) {
		this.serverResources = serverResources;
	}
}
