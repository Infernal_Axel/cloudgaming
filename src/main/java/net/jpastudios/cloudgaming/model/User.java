package net.jpastudios.cloudgaming.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/** Entity representing an user who can own playlists and have a current game.
 * This class contains user useful info for managing the cloud resources,
 * it doesn't contain login procedures or any other (unrelated) logic. */
@Entity
@Table(name = "users")
public class User {
	@Id
	@Column(name = "id_user")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_user;
    private String username;
    private String email;
    /** Date of birth */
    private LocalDate dob;
    
    /** The game to play currently selected by the player. */
    @ManyToOne(optional = true)
    @JoinColumn(name="id_game", nullable=true)
    private Game currentGame;
    
    /** Game playlists made by this user. This list contains every user's playlist, including sublists. */
    @OneToMany(mappedBy="owner", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY, orphanRemoval=true)
    private List<Playlist> playlists;

	public User() {
		playlists = new ArrayList<Playlist>();
	}

	public long getId_user() {
		return id_user;
	}

	public void setId_user(long id_user) {
		this.id_user = id_user;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public LocalDate getDob() {
		return dob;
	}
	
	public void setDob(LocalDate dob) {
		this.dob = dob;
	}
	
	public List<Playlist> getPlaylists() {
		return (List<Playlist>) Collections.unmodifiableList(playlists);
	}
	
	public void addPlaylist(Playlist playlist) {
		playlist.setOwner(this);
		this.playlists.add(playlist);
	}
	
	public void removePlaylist(Playlist playlist) {
		this.playlists.remove(playlist);
	}

	public Game getCurrentGame() {
		return currentGame;
	}

	public void setCurrentGame(Game currentGame) {
		this.currentGame = currentGame;
	}
}
