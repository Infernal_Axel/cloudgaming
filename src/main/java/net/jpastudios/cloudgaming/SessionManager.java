package net.jpastudios.cloudgaming;

import net.jpastudios.cloudgaming.model.Company;
import net.jpastudios.cloudgaming.model.Game;
import net.jpastudios.cloudgaming.model.Playlist;
import net.jpastudios.cloudgaming.model.ServerResources;
import net.jpastudios.cloudgaming.model.User;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/** This singleton class manages Hibernate Sessions and Transactions. 
 * Every service should call this class to open and close them. 
 */
public class SessionManager {
	public static final SessionManager instance = new SessionManager();
	
	private SessionFactory sessionFactory;
	private Session currentSession;
	private Transaction currentTransaction;
	/** Standard configuration file that loads Hibernate options */
	private String configFile = "hibernate.cfg.xml";
	
	protected SessionManager() {
		setup();
	}
	
	/** Call this method only for testing purposes. It loads the dedicated configuration. */
	public void loadTestingMode() {
		configFile = "hibernateTesting.cfg.xml";
		setup();
	}
	
	public SessionFactory getSessionFactory()
	{
		if(sessionFactory == null) 
		{
			setup();
		}
		
		return sessionFactory;
	}
	
	protected Configuration loadConfig() {
		Configuration configuration = new Configuration();
    	configuration.configure(configFile);
    	return configuration;
	}
	
	/** Setup Hibernate following the official procedure. */
	protected void setup() { 
		Configuration configuration = loadConfig();
		
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
		.applySettings(configuration.getProperties())
		.build();
		try {
			Metadata metadata = new MetadataSources(registry)
			.addAnnotatedClass(User.class)
			.addAnnotatedClass(Game.class)
			.addAnnotatedClass(Playlist.class)
			.addAnnotatedClass(Company.class)
			.addAnnotatedClass(ServerResources.class)
			.getMetadataBuilder()
		    .applyImplicitNamingStrategy(ImplicitNamingStrategyJpaCompliantImpl.INSTANCE)
		    .build();
			
			sessionFactory = metadata.getSessionFactoryBuilder().build();			
		} catch (Exception ex) {
			System.err.println("Problem creating session factory");
			ex.printStackTrace();
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}
	
	public void exit() {
    	if (sessionFactory != null) {	
            try {
                sessionFactory.close();
            } catch (HibernateException ignored) {
            	System.err.printf("Couldn't close SessionFactory", ignored);
            }
        }
    }
	
	public Session openCurrentSession() {
		currentSession = this.getSessionFactory().openSession();
		return currentSession;
	}
	
	public Session openCurrentSessionWithTransaction() {
		currentSession = this.getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}
	
	public void closeCurrentSession() {
		currentSession.close();
	}
	
	public void closeCurrentSessionWithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}
	
	public Session getCurrentSession() {
		return currentSession;
	}
	
	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}
	
	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}
	
	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}
	
	public static SessionManager getInstance() {
		return instance;
	}
}
